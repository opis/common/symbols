# Repository for Common Symbols

These repository should store commons symbols used by different sections

Use of these objects in other OPIs creates a dynamic dependency. Updates to this repository will automatically affect the appearance of the application uses cases.
